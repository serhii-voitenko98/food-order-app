import Header from "./components/layout/header/Header.component";
import React, {useState} from 'react';
import Meals from "./components/meals/Meals.component";
import Cart from "./components/cart/Cart.component";
import CartProvider from "./store/cart/CartProvider.component";

function App() {
    const [isCartOpened, setIsCartOpened] = useState(false);

    const cartProviderContext = {isCartOpened, setIsCartOpened};

    return (
        <CartProvider cartProviderContext={cartProviderContext}>
            {isCartOpened && <Cart/>}
            <Header></Header>
            <main>
                <Meals/>
            </main>
        </CartProvider>
    );
}

export default App;
