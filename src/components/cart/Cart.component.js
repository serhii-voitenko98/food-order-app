import {useContext} from "react";
import classes from './Cart.module.css';
import Modal from "../ui/modal/Modal.component";
import CartContext from "../../store/cart/cart-context";
import CartItem from "./CartItem.component";

const Cart = props => {
    const {setIsCartOpened, items, totalAmount: totalAmountNumber, updateItem} = useContext(CartContext);

    const onAddHandler = ({id, amount}) => {
        updateItem({id, amount: amount + 1});
    };
    const onRemoveHandler = ({id, amount}) => {
        updateItem({id, amount: amount - 1});
    };
    const cartItems = <ul className={classes['cart-items']}>{items.map(item => (
        <CartItem
            key={item.id}
            name={item.name}
            amount={item.amount}
            price={item.price}
            onAdd={onAddHandler.bind(null, item)}
            onRemove={onRemoveHandler.bind(null, item)}
        />
    ))}</ul>;
    const totalAmount = `$${totalAmountNumber.toFixed(2)}`;

    return (
        <Modal onClose={() => setIsCartOpened(false)}>
            {cartItems}
            <div className={classes.total}>
                <span>Total Amount</span>
                <span>{totalAmount}</span>
            </div>
            <div className={classes.actions}>
                <button
                    className={classes['button-alt']}
                    onClick={() => setIsCartOpened(false)}
                >
                    Close
                </button>
                {!!items.length && <button className={classes.button}>Order</button>}
            </div>
        </Modal>
    );
};

export default Cart;
