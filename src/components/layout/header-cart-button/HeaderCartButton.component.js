import classes from './HeaderCartButton.module.css';
import CartIcon from "../../cart/CartIcon.component";
import {useContext, useEffect, useState} from "react";
import CartContext from "../../../store/cart/cart-context";

const HeaderCartButton = props => {
    const cartContext = useContext(CartContext);
    const [isButtonHighlighted, setIsButtonHighlighted] = useState(false);

    const numberOfCartItems = cartContext.items.reduce((acc, item) => acc + item.amount, 0);

    const onClickHandler = () => {
        props.onClickHandler();
    };

    const buttonClasses = `${classes.button} ${isButtonHighlighted ? classes.bump : ''}`;

    useEffect(() =>  {
        if (cartContext.items.length <= 0) {
            return;
        }

        setIsButtonHighlighted(true);

        const timer = setTimeout(() => {
            setIsButtonHighlighted(false);
        }, 300);

        return () => {
            clearTimeout(timer);
        }
    }, [cartContext.items]);

    return (
        <button className={buttonClasses} onClick={onClickHandler}>
            <span className={classes.icon}>
                <CartIcon></CartIcon>
            </span>
            <span>Your Cart</span>
            <span className={classes.badge}>{numberOfCartItems}</span>
        </button>
    );
};

export default HeaderCartButton;
