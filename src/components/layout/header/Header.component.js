import React, {useContext} from 'react';
import mealsImage from '../../../assets/meals.jpg';
import classes from './Header.module.css';
import HeaderCartButton from "../header-cart-button/HeaderCartButton.component";
import CartContext from "../../../store/cart/cart-context";

const Header = props => {
    const { setIsCartOpened } = useContext(CartContext);

    return (
        <React.Fragment>
            <header className={classes.header}>
                <h1>ReactMeals</h1>
                <HeaderCartButton onClickHandler={() => setIsCartOpened(true)}></HeaderCartButton>
            </header>
            <div className={classes['main-image']}>
                <img src={mealsImage} alt="Meals" />
            </div>
        </React.Fragment>
    );
};

export default Header;
