import classes from './MealItemForm.module.css';
import Input from "../../ui/input/Input.component";
import {useRef} from "react";

const MealItemForm = props => {
    const amountInputRef = useRef();

    const onAddHandler = (event) => {
        event.preventDefault();
        const amount = Number(amountInputRef.current.value);
        if (amount < 1 || amount > 5) {
            return;
        }
        props.onAdd(amount);
    };

    return (
        <form className={classes.form}>
            <Input ref={amountInputRef} label="Amount" input={{
                id: 'amount',
                type: 'number',
                min: '1',
                max: '5',
                step: '1',
                defaultValue: '1'
            }}/>
            <button onClick={onAddHandler}>+ Add</button>
        </form>
    );
};

export default MealItemForm;
