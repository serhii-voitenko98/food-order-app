import React from 'react';
import MealsSummary from "./MealsSummary.component";
import AvailableMeals from "./AvailableMeals.component";

const Meals = props => {
    return (
        <React.Fragment>
            <MealsSummary/>
            <AvailableMeals/>
        </React.Fragment>
    )
};

export default Meals;