import React from "react";

const CartContext = React.createContext({
    items: [],
    totalAmount: 0,
    addItem: (item) => {},
    removeItem: (id) => {},
    isCartOpened: false,
    setIsCartOpened: () => null
});

export default CartContext;
