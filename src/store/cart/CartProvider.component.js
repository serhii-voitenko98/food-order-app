import CartContext from "./cart-context";
import {useReducer} from "react";

const defaultCartState = {
    items: [],
    totalAmount: 0
};

const ADD_ITEM_TO_CART_ACTION = 'ADD_ITEM_TO_CART';
const REMOVE_ITEM_FROM_CART_ACTION = 'ADD_ITEM_TO_CART';
const UPDATE_CART_ITEM_ACTION = 'UPDATE_CART_ITEM';

const calculateTotalAmount = items => items.reduce((acc, item) => acc + (item.price * item.amount), 0);

const cartReducer = (state, action) => {
    switch (action.type) {
        case ADD_ITEM_TO_CART_ACTION: {
            const updatedItems = state.items.filter(item => item.id === action.value.id).length > 0
                ? state.items.map(item => item.id === action.value.id ? {...item, amount: item.amount + 1} : item)
                : [...state.items, {...action.value}];

            const updatedTotalAmount = calculateTotalAmount(updatedItems);
            return {...state, items: updatedItems, totalAmount: updatedTotalAmount};
        }
        case REMOVE_ITEM_FROM_CART_ACTION: {
            const updatedItems = state.items.filter(item => item.id !== action.value);
            const updatedTotalAmount = calculateTotalAmount(updatedItems);
            return {...state, items: updatedItems, totalAmount: updatedTotalAmount};
        }
        case UPDATE_CART_ITEM_ACTION: {
            const updatedItems = state.items
                .map(item => action.value.id === item.id ? { ...item, ...action.value } : item)
                .filter(item => item.amount > 0);
            const updatedTotalAmount = calculateTotalAmount(updatedItems);
            return {...state, items: updatedItems, totalAmount: updatedTotalAmount};
        }
        default: {
            return {...defaultCartState};
        }
    }
};

const CartProvider = (props) => {
    const [cartState, dispatchCartAction] = useReducer(cartReducer, defaultCartState);
    const addItemToCartHandler = (item) => {
        dispatchCartAction({
            type: ADD_ITEM_TO_CART_ACTION,
            value: item
        });
    };
    const removeItemFromCartHandler = (id) => {
        dispatchCartAction({
            type: REMOVE_ITEM_FROM_CART_ACTION,
            value: id
        });
    };

    const updateItemCartHandler = (item) => {
        dispatchCartAction({
            type: UPDATE_CART_ITEM_ACTION,
            value: item
        });
    }

    const cartContext = {
        items: cartState.items,
        totalAmount: cartState.totalAmount,
        addItem: addItemToCartHandler,
        removeItem: removeItemFromCartHandler,
        updateItem: updateItemCartHandler,
        ...props.cartProviderContext
    };
    return (
        <CartContext.Provider value={cartContext}>
            {props.children}
        </CartContext.Provider>
    );
};

export default CartProvider;
